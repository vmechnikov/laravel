<?php

namespace App\Http\Controllers\Auth;

use App\Statistic;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|min:8|max:15|unique:users|regex:/\+?[0-9]{9}/', // |regex:/(01)[0-9]{9}/
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data

     */
    protected function create(array $data)
    {

		session(['name' => $data['name']]);
		session(['email' => $data['email']]);
		session(['password' =>  bcrypt($data['password'])]);
		session(['phone' => $data['phone']]);

		return redirect('popup');
    }

	public function register(Request $request)
	{
		$this->validator($request->all())->validate();
		event(new Registered($user = $this->create($request->all())));
		return redirect('popup');
	}

    public function popup(){
		$phone = session('phone');
		$email = session('email');

		$statistic = Statistic::where('phone', '=', $phone)->where('email', '=', $email)->first();
		if(isset($statistic)){
			$statistic->number_payments = $statistic->number_payments + 1;
			$statistic->save();
		} else {
			Statistic::create([
				'email' => $email,
				'phone' => $phone,
				'popup_open_number' => 1,
				'number_payments' => 1,
			]);
		}
    	return view("popup");
	}

	public function createComplete(){

    	$name = session('name');
    	$phone = session('phone');
    	$email = session('email');
    	$password = session('password');

		$user = User::create([
			'name' => $name,
			'phone' => $phone,
			'email' => $email,
			'password' => $password,
		]);

		Auth::login($user, true);

		return redirect('/home');
	}

	public function cancel(){
		$phone = session('phone');
		$email = session('email');

		$statistic = Statistic::where('phone', '=', $phone)->where('email', '=', $email)->first();
		if(isset($statistic)){
			$statistic->popup_open_number = $statistic->popup_open_number + 1;
			$statistic->save();
		} else {
			Statistic::create([
				'email' => $email,
				'phone' => $phone,
				'popup_open_number' => 1,
				'number_payments' => 1,
			]);
		}
		return response()->json(['status' => 'redirect']);
	}
}
