@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                        <br>
                        <br>
                        <p style="font-weight: bold;">Payment attempts: {{$numbersPayment}}<p>
                        <p style="font-weight: bold;">Pay clicked: {{$numbers}}<p>
                        <hr>

                        <h3>User</h3>
                        <table id="example" class="display" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>phone</th>
                                <th>email</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>phone</th>
                                <th>email</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->phone}}</td>
                                <td>{{$user->email}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <h3>PopUp look stat.</h3>
                        <table id="example2" class="display" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>phone</th>
                                <th>popup look number</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>phone</th>
                                <th>popup look number</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($pops as $p)
                                <tr>
                                    <td>{{$p->email}}</td>
                                    <td>{{$p->phone}}</td>
                                    <td>{{$p->popup_open_number}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    <hr>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
